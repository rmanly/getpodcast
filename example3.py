#! /usr/bin/env python3

import getpodcast
import subprocess
import pathlib
import shutil

def compressor(*args, **kwargs):
    podcast = kwargs["pod"]
    if podcast != "The Ron Burgundy Podcast":
        return

    if not shutil.which("ffmpeg"):
        getpodcast.message("ffmpeg not available")
        return

    uncompressed = pathlib.Path(kwargs["newfilename"])
    compr_folder = uncompressed.parent.joinpath("compr")

    if not compr_folder.exists():
        compr_folder.mkdir()

    compressed = compr_folder.joinpath(uncompressed.name)

    getpodcast.message("Compressing file ...")
    subprocess.run([
        "ffmpeg", "-v", "quiet", "-y",
        "-i", str(uncompressed),
        "-filter_complex",
        "compand=attacks=0:points=-80/-900|-45/-15|-27/-9|0/-7|20/-7:gain=5", 
        str(compressed)
    ])
    getpodcast.message("Compressing complete")
    getpodcast.message("Compressed filename:")
    getpodcast.message("    {}".format(compressed))

opt = getpodcast.options(
    date_from='2019-07-30',
    root_dir='./podcast',
    hooks='validated=compressor'
)

podcasts = {
    "The Ron Burgundy Podcast": "https://feeds.megaphone.fm/HSW7933892085"
}

getpodcast.getpodcast(podcasts, opt)
