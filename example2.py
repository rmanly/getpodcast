#! /usr/bin/env python3
import os
import getpodcast
from collections import OrderedDict

opt = getpodcast.options(
    date_from='2021-01-01',
    user_agent='Mozilla/5.0',
    root_dir=os.path.expanduser('~/podcast'),
    template="{rootdir}/{podcast}/{year}/{date} - {title}{ext}")

podcasts = {
    "SGU":"https://feed.theskepticsguide.org/feed/sgu",
    "Taskmaster":"https://feed.podbean.com/taskmasterpodcast/feed.xml",
    "Bad Voltage":"http://www.badvoltage.org/feed/mp3/",
    "Radiolab":"http://feeds.wnyc.org/radiolab",
    "Serial":"http://feeds.serialpodcast.org/serialpodcast?format=xml",
    "Neebscast":"https://www.blubrry.com/feeds/neebscast.xml",
}

podcasts = OrderedDict(sorted(podcasts.items(), key=lambda t: t[0]))

getpodcast.getpodcast(podcasts, opt)
